'use strict';

var fs = require('fs'),
    http = require('http'),
    https = require('https'),
    co = require('co'),
    forEach = require('co-foreach'),
    wait = require('co-wait'),
    cheerio = require('cheerio'),
    _ = require('underscore');

var request = require('request');

var knapsack = require('knapsack-js');
var solver = require("javascript-lp-solver");


var querystring = require('querystring');


function sendPostRequest(steamId){

        var jsonData = querystring.stringify({
                "stage":'bot',
                "steamId":  steamId,
                "hasBonus":"false",
                "coins":"0"
            });

        var agent = new https.Agent({keepAlive:true, keepAliveMsecs: 1000});

        var post_options = {
            host: 'csgosell.com',
            port: '443',
            method: 'POST',
            path: '/phpLoaders/getInventory/getInventory.php',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': jsonData.length,
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',
                'cookie':'__cfduid=dbc800824da21789178c9742bc573c0c51476563126; PHPSESSID=40717fdfb505e7e55a76a808c2948159; _gat=1; T2SteamID64=b5befc76ef626964e6e36ef55ef1b316; _ga=GA1.2.618315240.1476563134'				},
            agent:agent
        };

        return new Promise( (resolve, reject) => {

            var post_req = https.request(post_options, function(res) {
            var result = '';



            res.on('data', (chunk) => {result += chunk});



            res.on('end', () => {resolve(result)});


            res.on('error', err => {reject(err)});


        });


        post_req.on('error', (err) => {reject(err)});


        post_req.write(jsonData);
        post_req.end();



    });


}






function sendOffer(botOffer, botID){


        var jsonData = querystring.stringify({
            json: JSON.stringify({
                clientOfferArray:[{"amount":"1","classId":"613589848","name":"Operation Breakout Case Key"}],
                botOfferArray: botOffer
            }),
            id:'76561197961911625',
            token:'gqvUXgz8',
            botNumber:botID
        });


        var agent = new https.Agent({keepAlive:true, keepAliveMsecs: 1000});

        var post_options = {
            host: 'csgosell.com',
            port: '443',
            method: 'POST',
            path: '/phpLoaders/tradeOfferVerification/tradeOfferVerification.php',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Content-Length': jsonData.length,
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',
            },
            agent:agent
        };

        return new Promise( (resolve, reject) => {

            var post_req = https.request(post_options, function(res) {
                var result = '';



                res.on('data', (chunk) => {result += chunk});



                res.on('end', () => {resolve(result)});


                res.on('error', err => {reject(err)});


            });


            post_req.on('error', (err) => {reject(err)});


            post_req.write(jsonData);
            post_req.end();
        });


}





function getOpskinsPrices(){

    var url = 'https://api.opskins.com/IPricing/GetAllLowestListPrices/v1/?appid=730';

    return new Promise( (resolve, reject) => {


            request.get(url, function(error, response, body){
                if (!error)
                    resolve(JSON.parse(body.toString()));
                else
                    reject(error);
            });


    });

}


function getOpskinsHistory(){
	var url = 'https://api.opskins.com/IPricing/GetPriceList/v1/?appid=730';

    return new Promise( (resolve, reject) => {


            request.get(url, function(error, response, body){
                if (!error)
                    resolve(JSON.parse(body.toString()));
                else
                    reject(error);
            });


    });
}

function notify(header, text){
	var url = 'https://pushall.ru/api.php?type=broadcast&id=2563&key=06e253e51af05b00818bde45e912176c&title=' + encodeURI(header) +'&text=' + encodeURI(text);

    return new Promise( (resolve, reject) => {


            request.get(url, function(error, response, body){
                if (!error)
                    resolve(body);
                else
                    reject(error);
            });


    });
}




function getCSM(){
	var time = new Date();
		time = Math.floor(time/1000);


	var options = {
		url: 'http://cs.money/load_all_bots_inventory?hash=' + time,
		headers: {
			'Cookie': '__cfduid=da0f82c3f70520121a799895a89a1ba021476568080; _ym_uid=1476568086414508177; _ym_isad=1; language=en; _gat=1; steamid=76561197961911625; avatar=https%3A%2F%2Fsteamcdn-a.akamaihd.net%2Fsteamcommunity%2Fpublic%2Fimages%2Favatars%2Fec%2Fec294d0a8aeacdfa94beef73866183e4615b6e7b_medium.jpg; username=L1vet%20CSGOSell.com%20CS.MONEY; after_auth=true; csrf=Mw2FlwTT7sljos9oTbXAGQ3ebgJkC3LnY60ntE5agOE%3D; trade_link=https%3A%2F%2Fsteamcommunity.com%2Ftradeoffer%2Fnew%2F%3Fpartner%3D1645897%26token%3DgqvUXgz8; connect.sid=s%3AMRY_4P6QU5Sf9dw262ulLrjyOnL43R9t.M0T5jD51XyGWbOKHPolxrsMSIE%2B5zrIENykMpv0iRe0; _ga=GA1.2.1227797687.1476568086'
		}
	};

    return new Promise( (resolve, reject) => {


            request.get(options, function(error, response, body){
                if (!error){
                	//fs.writeFileSync('debug.json', body.toString())
                    resolve(JSON.parse(body.toString()));
                }
                else
                    reject(error);
            });


    });
}





function getBotItems(html){
    var $ = cheerio.load(html.toString());

    var items =  [];


    $('div.itemImgDiv').each(function(){
        var item = $(this);
        var price = item.attr('value');
            //price = price.substr(1);

        var id = item.attr('id');
        var amount = item.attr('amount');
        var classId = item.attr('classid');

        var name = item.attr('original-name');
        items.push({market_hash_name: name, price: price, id:id, amount: amount, classId: classId})
    });

    return items;

}


function getProfit(items, prices, history, KeyPrice, myKeyPrice){

    var simpleVAR = {};
    var simpleINT = {};

     var simpleCON = {
        tradePrice: {max: KeyPrice}

    };

    var ignore = ['Music Kit | Various Artists, Hotline Miami', 'StatTrak™ MAC-10 | Tatter (Well-Worn)', 'Sealed Graffiti | Heart (Bazooka Pink)', 'Sealed Graffiti | Eco (Blood Red)', 'SG 553 | Tiger Moth (Factory New)', 'CZ75-Auto | The Fuschia Is Now (Field-Tested)'];
    var profits = {};
     for (var key in items){

            var item = items[key];


            if(item.price < KeyPrice && item.price > 0 && prices.response[item.market_hash_name] && item.market_hash_name.match(/StatTrak/) == null && item.market_hash_name.match(/MLG/) == null && item.market_hash_name.match(/Souvenir/) == null && item.market_hash_name.match(/Sticker/) == null && item.market_hash_name.match(/Sealed/) == null && item.market_hash_name.match(/Autograph/) == null && item.market_hash_name.match(/Cologne/) == null && item.market_hash_name.match(/Monkey Business/) == null){


            var flag = item.market_hash_name.match(/P250 | Supernova/);
                    var price = prices.response[item.market_hash_name].price;

					var minSellPrice = [];
					for(var key in history[item.market_hash_name]) minSellPrice.push(history[item.market_hash_name][key].price);


						minSellPrice = minSellPrice.slice(minSellPrice.length-8, minSellPrice.length-1);

						minSellPrice = _.min(minSellPrice);
						if(minSellPrice < price) price = minSellPrice;

						price = price/100;

                    var profit = price - (myKeyPrice * item.price / KeyPrice) * 1.1;

                    if(profit > 0 &&  item.price >= price && ignore.indexOf(item.market_hash_name)==-1){






						var amount;
                        if(item.amount) amount = parseInt(item.amount); // Возможно тут ошибка с количеством, нужно проверить
                        else amount = 1;



						if(profits[item.classId] == undefined){
							simpleINT[item.classId] = 1;
							profits[item.classId] = {name: item.market_hash_name, profit: profit, price: price, tradePrice: item.price, id: item.id, amount: amount, classId:item.classId };

							simpleVAR[item.classId] = {profit: profit, tradePrice: item.price};
							simpleVAR[item.classId]['id' + item.id] = 1;
							simpleCON['id' + item.id] = {max:amount};


						}
						else{
							//console.log(profits[item.market_hash_name].id+JSON.stringify(simpleCON['id' + profits[item.market_hash_name].id], '', 5))
                            console.log(1000);
							profits[item.classId]['amount'] =  profits[item.classId]['amount'] + amount;
							simpleCON['id' + profits[item.classId].id]['max'] = simpleCON['id' + profits[item.classId].id]['max'] + amount;
                        }


                    }




            }


    }

    var model = {
        "optimize": "profit",
        "opType": "max",
        "constraints": simpleCON,
        "variables": simpleVAR,
        "ints": simpleINT
    }





    var results = solver.Solve(model);

    var profit = results.result;

    delete results.result;
    delete results.feasible;

    var classIds = Object.keys(results);



    var choose = [];
    var script = '';
    var total = 0.0;


    var csgoselloffers = [];

	

    classIds.forEach(function(key){
         if(results[key] > 0){
            var amount = 0;
			var i = 0;
            for(; i < results[key]; i++)script += 'addElement(' + profits[key].id + ', "botsIv");';
			amount += parseInt(i);
			profits[key].amount = i;
            choose.push(profits[key]);
            if(flag)console.log('total: ' + total + 'price: ' + parseFloat(profits[key].tradePrice)*i)
            total += parseFloat(profits[key].tradePrice)*i;

            csgoselloffers.push(_.pick(profits[key], ['name', 'amount', 'classId']));
        }
    })



    return {choose: choose, profit: profit, total:total, script:script, amount:amount, csgoselloffers: csgoselloffers};





}

var bots;
var gresult = {};
var maxProfit = 0;

var calc = co.wrap(function* (){
    var result = {profit:0};


    var opskins = yield getOpskinsPrices();

	var history = yield getOpskinsHistory();
		history = history.response;

   bots = ['76561198203778081', '76561198284997423', '76561198339297146', '76561198339767074', '76561198340737213'];
   

    yield forEach(bots, function*(bot, i) {

        var html = yield sendPostRequest(bot);
       //fs.writeFileSync(bot + '.html', html);
        var items = getBotItems(html);

        var opPrice = process.env.opPrice || 2.23;
        var keyPrice = process.env.keyPrice || 2.83;


        var localRes = getProfit(items, opskins, history, keyPrice, opPrice);
        localRes.id = i + 1;
        localRes.site = 'CSGOsell.com';

        if (localRes.profit > result.profit) result = localRes;


    });





	if(result.profit > 0.3){


        var offer = yield sendOffer(result.csgoselloffers, result.id);

		var header = 'Тут выгодный обмен появился'
		var text =  '';/*Site: ' + result.site + '\n*/
			text += 'Profit: ' + result.profit + '\n';
			text += 'Total: ' + result.total + '\n';
			text += 'Amount: ' + result.amount + '\n';
            text += 'Offer: ' + offer  + '\n';
			//text += 'Bot: ' + result.id + '\n';
            text += 'Skins: ' + result.choose.length  + '\n';

		for(var i in result.choose)
			text += result.choose[i].name + ' ' + result.choose[i].amount + 'x' + ' ' + result.choose[i].profit + '$\n';


        console.log(offer);

		yield notify(header, text);
	}

    


	gresult = result;
	return result;
});



var calc2 = function(){
calc()
.then(result => {

	 var date = new Date();


	if(result.choose){
			maxProfit = result.profit;



			 	console.log(result.choose);


				console.log('Time:  ' + date.getHours() + ':' + date.getMinutes());
				console.log('Profit:  ' + result.profit);
				console.log('Total:  ' + result.total);
				console.log('Site:  ' + result.site);
				console.log('Bot:  ' + result.id);
				console.log('Quantity:  ' + result.choose.length);
				console.log('Amount:  ' + result.amount);
				console.log('Script:  ' + result.script); 



	 }
	return;

})
.catch(err => console.log('trade: ' + err.stack));
}

//showInventoryBot(array_skins_bot_global, '', 'mefedkostyamb');

var koa = require('koa');

var app = koa();



app.use( function *(){
	yield calc();
	if(gresult.site){
		var text =  'Site: ' + gresult.site + '\n';
			text += 'Profit: ' + gresult.profit + '\n';
			text += 'Total: ' + gresult.total + '\n';
			text += 'Amount: ' + gresult.amount + '\n';
			text += 'Bot: ' + gresult.id + '\n';
			text += 'Skins: ' + gresult.choose.length  + '\n';

		for(var i in gresult.choose)
			text += gresult.choose[i].name + ' ' + gresult.choose[i].amount + 'x' + ' ' + gresult.choose[i].profit + '            ' + gresult.choose[i].tradePrice + '$\n';


	}
	else
		text = 'Wait please!';
		this.body = text;
});


app.on("error", (err) => {});

var port = process.env.PORT || 5000;
var port = process.env.OPENSHIFT_NODEJS_PORT || 5000;
var ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

app.listen(port, ip_address);
calc2();
setInterval(calc2, 60000);

module.exports = app ;
